﻿using System.Linq;
using System.Collections.Generic;
using UnityEngine;

public class Tail : MonoBehaviour
{
	public Player Player;
	int position = 0;		
    void Update()
    {
        if(Player != null)
		{
			if(position < Player.LastTurn.Count)
			{
				transform.position = Vector3.MoveTowards(transform.position, Player.LastTurn[position], Statics.Speed * Time.deltaTime);
				if(Vector3.Distance(transform.position, Player.LastTurn[position]) < 0.1f)
				{
					position++;
				}
			}
			else
			{
				transform.position = Vector3.MoveTowards(transform.position, Player.transform.position, Statics.Speed * Time.deltaTime);
			}
		}
    }
	void OnTriggerEnter2D(Collider2D collider)
	{
		Player.GameOver.SetActive(true);
	}
}
