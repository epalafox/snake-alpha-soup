﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Letter : MonoBehaviour
{
	void OnTriggerEnter2D(Collider2D collider)
	{
		collider.GetComponent<Player>().Add(GetComponent<TextMesh>().text);
		Destroy(gameObject);
	}
}
