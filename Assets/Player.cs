﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
	TextMesh text;
	GameObject letter;
	public Vector3 direction;
	public List<Vector3> LastTurn;
	public GameObject GameOver;
    void Start()
    {
		text = GetComponent<TextMesh>();
        text.text = Statics.List[Random.Range(0, Statics.List.Count)];
		direction = Vector3.right;
		LastTurn.Add(transform.position);
    }
    void Update()
    {
        if(letter == null)
		{
			var vector = new Vector3(Camera.main.ViewportToWorldPoint(new Vector3(Random.Range(0.1f, 0.9f), 0f, Camera.main.nearClipPlane)).x, 
				Camera.main.ViewportToWorldPoint(new Vector3(0, Random.Range(0.1f, 0.9f), Camera.main.nearClipPlane)).y, transform.position.z);
			letter = Instantiate<GameObject>(Resources.Load<GameObject>("Element"), vector, Quaternion.identity);
			letter.GetComponent<TextMesh>().text = Statics.List[Random.Range(0, Statics.List.Count)];
		}
		var newDirection = direction;
		if(Input.GetAxis("Horizontal") != 0)
		{
			newDirection = Input.GetAxis("Horizontal") < 0 ? Vector3.left : Vector3.right;
		}
		if(Input.GetAxis("Vertical") != 0)
		{
			newDirection = Input.GetAxis("Vertical") < 0 ? Vector3.down : Vector3.up;
		}
		if(newDirection != direction)
		{
			LastTurn.Add(transform.position);
			direction = newDirection;
		}
		transform.position += newDirection * Statics.Speed * Time.deltaTime;
    }
	public void Add(string letter)
	{
		var t = Instantiate<GameObject>(Resources.Load<GameObject>("Tail"), LastTurn[0], Quaternion.identity);
		t.transform.SetParent(null, true);
		var tail = t.GetComponent<Tail>();
		tail.GetComponent<TextMesh>().text = letter;
		tail.Player = this;
	}
}
