﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EndGame : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
		Time.timeScale = 0;
    }
	public void Restart()
	{
		Time.timeScale = 1;
		SceneManager.LoadScene("Main");
	}
}
